# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

## README:
-------------

Evan Kender

1304 SE 46th Ave

Portland, OR

97215

This software parses through a config file "credentials.ini" for a message.

It then prints this message.
